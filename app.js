var express = require('express');
var request = require('request');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var url = require('url');
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(__dirname +'/public'));


app.get('/', function (req, res) {
    res.sendfile("index.html");
});

app.get('/search', function (req, res) {
    console.log(req.url)
    var query = url.parse(req.url, true).query;
    console.log(query);
    var YouTube = require('youtube-node');
    var youTube = new YouTube();
    youTube.setKey('AIzaSyCUI7cgpYUo5JZUeMvBjHzNiubdThjenVA');
    youTube.search(query.filter, query.output, function (error, result) {
        if (error) {
            res.status(500).send('Error');
        }
        else {
           /* console.log(result);*/
            res.send(JSON.stringify(result, null, 2))
        }
    });
});


app.listen(8082);
